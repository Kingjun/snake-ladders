//: Playground - noun: a place where people can play

import UIKit

class Player {
    var name: String
    var position: Int = 0
    var skipTurn: Bool = false
    init(name: String) {
        self.name = name
    }
}

var king: Player = Player(name: "King")
var zun: Player = Player(name: "Zun")

var players: [Player] = [king, zun]
players[0].name = "zun"
players[0].position = players[0].position + 10

var round: Int = 0
var board: [Int: Int] = [:]

func boardSetup() {
    board [3] = 10
    board [4] = 10
    board [5] = 11
    board [6] = 10
    board [11] = 50
    board [10] = -20
    board [15] = -3
    board [50] = 99
    board [94] = -93
    board [95] = -94
    board [96] = -95
    board [97] = -96
    
}

boardSetup()

func rollDice(numberOfDice: Int) -> Int {
    var diceTotal: Int = 0
    
    for (var i = 0; i < numberOfDice; i++) {
        let dice: Int = Int(arc4random_uniform(6)) + 1
        diceTotal = diceTotal + dice
    }
    
    return diceTotal
    
}

while (players[0].position < 100 && players[1].position < 100) {
    
    //Round Check
    round = round + 1
    print("Round no\(round)")
    
    for (var i=0; i<players.count; i++) {
        players[i].position = players[i].position + rollDice(1)
    
        //roll Dice
        var rolledDice: Int = rollDice(1)
        print ("Player \(i) rolled dice \(rolledDice), now at position \(players[i].position)")
        
        if let snakeOrLadders = board[players[i].position] {
        players[i].position = players[i].position + snakeOrLadders
        print ("Player \(i) landed on \(snakeOrLadders)")
        }
    }
}

